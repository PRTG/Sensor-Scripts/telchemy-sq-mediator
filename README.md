# Telchemy SQMediator

**Custom Python Sensors For SQMediator**

This archive contains a sample set of Python Script Sensors for PRTG that will retrieve health and performance data from a unified communications infrastructure that is managed by Telchemy SQMediator.

You can find out more about SQMediator here: https://www.telchemy.com/sqmediator.php

Additional information about how to use the sensors can be found here:

https://blog.paessler.com/monitoring-unified-communication-quality-with-prtg


**Installation:**

The .py files should be copied to the “Python” subfolder within the PRTG “Custom Sensors” folder (e.g. - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\python)
The .ovl (PRTG lookup) should be copied to C:\Program Files (x86)\PRTG Network Monitor\lookups, and the lookup files be refreshed using the “Admin Tools” tab in PRTG.
